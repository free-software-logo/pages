![](favicon.png)

# Free Software Logo

The Free Software Logo is released by Robert Martinez under [Creative Commons Zero 1.0](https://creativecommons.org/publicdomain/zero/1.0/).  
It is supposed to only indicate Free Software as [defined by the Free Software Foundation](https://www.gnu.org/philosophy/free-sw.html).

Please **DO**:
* use the logo wherever you need to represent Free Software
* colorize the Logo to fit your design requirements

Please **DO NOT**:
* deform the logo by stretching, compressing or skewing
